const pick = require('lodash.pick')

module.exports = function buildTransformation (input) {
  const T = {}

  Object.entries(input).forEach(([key, value]) => {
    switch (key) {
      case 'id': return
      case 'type': return

      case 'tombstone':
        T[key] = pick(value, ['date', 'reason'])
        T[key].date = Number(T[key].date)
        // graphql only allows 32bit signed Ints
        // so we're passing a Date and converting it to Int for ssb
        return

      case 'image':
        T[key] = pick(value, ['blob', 'unbox', 'mimeType', 'size', 'width', 'height'])
        return

      case 'authors':
        T[key] = {}
        if (value.add && value.add.length) T[key].add = value.add
        if (value.remove && value.remove.length) T[key].remove = value.remove
        return

      default:
        T[key] = value
    }
  })
  return T
}
