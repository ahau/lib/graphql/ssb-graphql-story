const { promisify: p } = require('util')

const Get = require('./queries/get')
const BulkGet = require('./queries/bulk-get')
const Save = require('./mutations/save')

// links
const GetLinks = require('./queries/get-links')

// spec types / tangles
const { STORY, COLLECTION, LINK } = require('../lib/constants')

module.exports = function (sbot, externalGetters) {
  const getStory = Get(sbot.story.get, STORY)
  const getStories = BulkGet(sbot, STORY, getStory)

  const getCollection = Get(sbot.story.collection.get, COLLECTION)
  const getCollections = BulkGet(sbot, COLLECTION, getCollection)

  // links
  const getLinkedStoriesByCollection = (id, cb) => GetLinks(sbot, getStory, STORY)({ collectionId: id }, cb)
  const getLinkedCollectionsByStory = (id, cb) => GetLinks(sbot, getCollection, COLLECTION)({ storyId: id }, cb)

  const postSaveStory = Save({
    create: sbot.story.create,
    update: sbot.story.update
  }, STORY)

  const postSaveCollection = Save({
    create: sbot.story.collection.create,
    update: sbot.story.collection.update
  }, COLLECTION)

  const postSaveCollectionStoryLink = Save({
    create: sbot.story.collection.createStoryLink,
    update: sbot.story.collection.updateStoryLink
  }, LINK)

  return {
    getStory: p(getStory),
    getStories: p(getStories),
    postSaveStory: p(postSaveStory),

    getCollection: p(getCollection),
    getCollections: p(getCollections),
    postSaveCollection: p(postSaveCollection),

    // links
    postSaveCollectionStoryLink: p(postSaveCollectionStoryLink),
    getLinkedStoriesByCollection: p(getLinkedStoriesByCollection),
    getLinkedCollectionsByStory: p(getLinkedCollectionsByStory),

    gettersWithCache: {
      getStory // doesn't currently have cache but likely will!
    }
  }
}
