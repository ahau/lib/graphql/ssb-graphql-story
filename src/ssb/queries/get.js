const { isMsg } = require('ssb-ref')

module.exports = function Get (get, tangle) {
  return function (id, cb) {
    if (!isMsg(id)) return cb(new Error(`${tangle} query expected %msgId, got ${id}`))

    get(id, (err, getted) => {
      if (err) return cb(err)

      cb(null, {
        ...getted,
        id,
        type: getted.type.replaceAll('story/', '').replaceAll('collection/', '')
      })
    })
  }
}
