const tape = require('tape')
const TestBot = require('../test-bot')
const { isMsgId } = require('ssb-ref')
const { SaveStory } = require('../lib/helpers')

tape('saveStory (create)', async (t) => {
  t.plan(3)
  const { ssb, apollo } = await TestBot({ loadContext: true })

  const save = SaveStory(apollo, t)

  const whoami = await apollo.query({
    query: `{
      whoami { 
        public {
          feedId
          profileId
        }
        personal {
          groupId
          profileId
        }
      } 
    } `
  })

  t.error(whoami.errors, 'whoami returns no errors')

  const { personal } = whoami.data.whoami

  const authors = {
    add: [ssb.id]
  }

  const storyId = await save({ type: 'test', recps: [personal.groupId], authors })

  t.true(
    isMsgId(storyId),
    'saves a minimal story, returning storyId'
  )

  ssb.close()
})

tape('saveStory (update)', async (t) => {
  t.plan(6)
  const { ssb, apollo } = await TestBot()

  const save = SaveStory(apollo, t)

  const storyId = await save({
    type: '*',

    title: 'Some title',
    description: 'this is a story',
    timeInterval: '2019/2020',
    submissionDate: '2020-05-XX',
    location: 'New Zealand',
    locationDescription: 'Some location description',
    contributionNotes: 'Some contribution notes',
    format: 'Some format',
    identifier: 'Some identifier',
    language: 'English',
    source: 'Some source',
    transcription: 'Some transcription',
    permission: 'view',
    authors: {
      add: [ssb.id]
    },
    recps: [ssb.id]
  })

  t.true(
    isMsgId(storyId),
    'saves a fully featured story, returning storyId'
  )

  const initialGet = await apollo.query({
    query: `query($input: ID!) {
      story(id: $input) {
        type
        title
        description
        timeInterval
        submissionDate
        location
        locationDescription
        contributionNotes
        format
        identifier
        language
        source
        transcription
        permission
        
        recps
      }
    }`,
    variables: {
      input: storyId
    }
  })

  t.deepEqual(
    initialGet.data.story,
    {
      type: '*',
      title: 'Some title',
      description: 'this is a story',
      timeInterval: '2019/2020',
      submissionDate: '2020-05-XX',
      location: 'New Zealand',
      locationDescription: 'Some location description',
      contributionNotes: 'Some contribution notes',
      format: 'Some format',
      identifier: 'Some identifier',
      language: 'English',
      source: 'Some source',
      transcription: 'Some transcription',
      permission: 'view',

      recps: [ssb.id]
    },
    'can retrieve story'
  )

  /* update */
  const update = await save({
    id: storyId, // << this makes it an update

    title: 'Some New title',
    location: 'Not in New Zealand',
    locationDescription: 'Some New location description'
  })

  t.error(update.errors, 'saves update with no errors')

  const updatedGet = await apollo.query({
    query: `query($input: ID!) {
      story(id: $input) {
        type

        title
        location
        locationDescription
      }
    }`,
    variables: {
      input: storyId
    }
  })

  t.deepEqual(
    updatedGet.data.story,
    {
      type: '*',
      title: 'Some New title',
      location: 'Not in New Zealand',
      locationDescription: 'Some New location description'
    },
    'can update story'
  )

  ssb.close()
})
